#include <stdlib.h>
#include <stdio.h>
#include "log.h"

// Output a hexadecimal string.
void log_printHex(unsigned char *bytes, int n) {
    int i;

    for (i = 0; i < n; i++)
        log_printf("%02x", bytes[i]);

    log_printf("\n");
}
