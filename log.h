#ifndef LOG_H
#define LOG_H

#include <stdlib.h>
#include <stdio.h>

#define log_printf(...) fprintf(stderr, __VA_ARGS__)
void log_printHex(unsigned char *bytes, int n);

#endif
