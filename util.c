#include <complex.h>
#include "util.h"

// Get the value of Nth bit in bytes.
// MSB first.
int getBit(unsigned char *bytes, int bit) {
    return (bytes[bit >> 3] >> (7 - (bit & 7))) & 1;
}

// Get a bit field.
int getBits(unsigned char *bytes, int start, int end) {
    int i, x;

    // Shift in bits one by one.
    x = 0;
    for (i = start; i < end; i++) {
        x <<= 1;
        x |= getBit(bytes, i);
    }

    return x;
}

// Limit an integer to a range.
int clampInt(int x, int minVal, int maxVal) {
    if (x < minVal)
        return minVal;
    if (x > maxVal)
        return maxVal;

    return x;
}

// Limit a double to a range.
double clampDouble(double x, double minVal, double maxVal) {
    if (x < minVal)
        return minVal;
    if (x > maxVal)
        return maxVal;

    return x;
}

// Squared magnitude.
double squaredMagnitude(double complex x) {
    return creal(x) * creal(x) + cimag(x) * cimag(x);
}
