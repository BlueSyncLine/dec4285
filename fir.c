#include <math.h>
#include <complex.h>
#include <malloc.h>

#include "fir.h"
#include "log.h"

// Sinc function.
double sinc(double x) {
    return x == 0.0 ? 1.0 : sin(x) / x;
}

// Compute the Blackman window.
double blackman(int M, int n) {
    return 0.42 - 0.5 * cos(M_PI * 2.0 * n / M) + 0.08 * cos(M_PI * 4.0 * n / M);
}

// Compute the taps for a FIR lowpass filter.
void fir_lowpass_taps(double complex *taps, int n, double fc) {
    int i, j, numTaps;
    double complex acc;

    // Total taps.
    numTaps = 2 * n + 1;

    // Compute the taps.
    j = 0;
    acc = 0.0;
    for (i = -n; i <= n; i++) {
        taps[j] = sinc(M_PI * 2.0 * i * fc) * blackman(numTaps + 1, j + 1);
        acc += taps[j];
        j++;
    }

    // Normalize.
    for (i = 0; i < numTaps; i++)
        taps[i] /= acc;

    log_printf("fir_lowpass_taps: n=%d, fc=%f\n", n, fc);
    for (i = 0; i < numTaps; i++)
        log_printf("%.12f, ", creal(taps[i]));
    log_printf("\n");
}

// Computes the taps of a raised-cosine filter.
void fir_rrc_taps(double complex *taps, int n, double tsi, double beta) {
    int i, j;
    double divisor;

    // Compute the taps.
    // The formula is described in https://commons.wikimedia.org/wiki/File:Root-raised-cosine-impulse.svg.
    // Worth pointing out is the fact that the main page has an incorrect one at the time of writing this (30 Jan 2021).
    j = 0;
    for (i = -n; i <= n; i++) {
        // fc = 1/Ts
        if (i == 0) {
            taps[j] = sqrt(tsi) * (1.0 - beta + (4.0 * beta / M_PI));
        } else {
            divisor = M_PI * i * tsi * (1.0 - pow(4.0 * beta * i * tsi, 2.0));

            // Prevent division by zero (this shouldn't occur, but is done for consistency).
            if (divisor != 0.0) {
                taps[j] = sqrt(tsi) * (sin(M_PI * i * tsi * (1.0 - beta)) + 4.0 * beta * i * tsi * cos(M_PI * i * tsi * (1.0 + beta))) / divisor;
            } else {
                taps[j] = beta * sqrt(tsi / 2.0) * ((1 + M_2_PI) * sin(M_PI_4 / beta) + (1 - M_2_PI) * cos(M_PI_4 / beta));
            }
        }
        j++;
    }

    log_printf("fir_rrc_taps: n=%d, tsi=%f, beta=%f\n", n, tsi, beta);
    for (i = 0; i < 2 * n + 1; i++)
        log_printf("%.12f, ", creal(taps[i]));
    log_printf("\n");
}

// Feed a sample into the FIR filter.
void fir_feed(firfilt_t *filt, double complex x) {
    // Place the current sample in the delay line.
    filt->delay[filt->currentIndex++] = x;

    // Wrap.
    filt->currentIndex %= filt->numTaps;
}

// Multiply-accumulate.
double complex fir_mac(firfilt_t *filt) {
    int i, j;
    double complex acc;

    // Multiply-accumulate.
    acc = 0.0;
    for (i = 0; i < filt->numTaps; i++) {
        j = (filt->currentIndex + filt->numTaps - i - 1) % filt->numTaps;

        // Tweak: conjugate to allow using the NLMS algorithm directly.
        acc += filt->delay[j] * conj(filt->taps[i]);
    }

    return acc;
}

// Simple pass-through.
double complex fir_next(firfilt_t *filt, double complex x) {
    fir_feed(filt, x);
    return fir_mac(filt);
}

// Attempt to allocate an empty FIR filter.
int fir_zero_alloc(firfilt_t *filt, int numTaps) {
    // Preset to NULL so that free can be called right away.
    filt->taps = NULL;
    filt->delay = NULL;

    // Initialize.
    filt->currentIndex = 0;

    // Set the number of taps.
    filt->numTaps = numTaps;

    // Allocate the taps array.
    if (!(filt->taps = calloc(filt->numTaps, sizeof(double complex)))) {
        fir_free(filt);
        return 0;
    }

    // Allocate the delay line.
    if (!(filt->delay = calloc(filt->numTaps, sizeof(double complex)))) {
        fir_free(filt);
        return 0;
    }

    return 1;
}

// Reset a FIR filter.
void fir_reset(firfilt_t *filt) {
    int i;
    filt->currentIndex = 0;

    for (i = 0; i < filt->numTaps; i++)
        filt->delay[i] = 0.0;
}

// Create a low-pass FIR filter, returns 0 on failure.
// It'll occupy 2n + 1 taps.
int fir_lowpass_alloc(firfilt_t *filt, double sampleRate, double cutoff, int n) {
    // Try allocating memory.
    if (!fir_zero_alloc(filt, n * 2 + 1))
        return 0;

    // Compute the filter.
    fir_lowpass_taps(filt->taps, n, cutoff / sampleRate);

    return 1;
}

// Create a root-raised-cosine FIR filter, returns 0 on failure.
// It'll occupy 2n + 1 taps.
int fir_rrc_alloc(firfilt_t *filt, double sampleRate, double baudRate, double beta, int n) {
    // Try allocating memory.
    if (!fir_zero_alloc(filt, n * 2 + 1))
        return 0;

    // Compute the filter.
    fir_rrc_taps(filt->taps, n, baudRate / sampleRate, beta);

    return 1;
}


// Deallocate the memory used.
void fir_free(firfilt_t *filt) {
    free(filt->taps);
    free(filt->delay);
}
