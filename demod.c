#include <stdint.h>
#include <math.h>
#include <complex.h>
#include "demod.h"
#include "fir.h"
#include "nlms.h"
#include "log.h"

// Phase to bit mapping.
// These values are sent MSB first.
const int DEMOD_4PSK_MAPPING[4] = {
    0, 1, 3, 2
};

const int DEMOD_8PSK_MAPPING[8] = {
    1, 0, 2, 3, 7, 6, 4, 5
};

// Attempts to allocate memory for the symbol demodulator.
// Returns 1 on success, 0 on failure.
int demod_alloc(demod_t *demod, int bitsPerSymbol, demod_resetCallback *resetCallback, demod_symbolCallback *symbolCallback) {
    int i, syncLFSR, lfsrFeedback;

    // Don't call reset when not in use.
    demod->resetCallback = NULL;
    demod->symbolCallback = symbolCallback;
    demod->bitsPerSymbol = bitsPerSymbol;

    // Allocate the filters.
    if (!fir_rrc_alloc(&demod->resamplingFilter, DEMOD_RATE, DEMOD_BAUD_RATE, DEMOD_RRC_BETA, DEMOD_RRC_N))
        return 0;

    if (!fir_zero_alloc(&demod->syncFilter, DEMOD_SYNC_LENGTH)) {
        fir_free(&demod->resamplingFilter);
        return 0;
    }

    if (!fir_zero_alloc(&demod->equalizerFilter, DEMOD_EQUALIZER_LENGTH)) {
        fir_free(&demod->resamplingFilter);
        fir_free(&demod->syncFilter);
        return 0;
    }

    // Initialize the variables.
    demod_reset(demod);

    // Set the callback now.
    demod->resetCallback = resetCallback;

    // Initialize the synchronization filter.
    // STANAG 4285, A-8
    // Note that the order of the sequence elements is reversed to match how the taps are applied.
    syncLFSR = 0x1a;
    for (i = DEMOD_SYNC_LENGTH - 1; i >= 0; i--) {
        demod->syncFilter.taps[i] = syncLFSR & 1 ? -1.0 : 1.0;
        lfsrFeedback = ((syncLFSR >> 2) ^ syncLFSR) & 1;
        syncLFSR >>= 1;
        syncLFSR |= lfsrFeedback << 4;
    }

    return 1;
}

// Resets the demodulator and starts a new acquisition.
void demod_reset(demod_t *demod) {
    int i;

    // Initialize the variables.
    demod->loFrequency = DEMOD_CARRIER_FREQUENCY / INPUT_RATE;
    demod->loPhase = 0.0;
    demod->agcLevel = DEMOD_AGC_MAX;

    demod->syncMagnitudeEarly = 0.0;
    demod->syncMagnitudeCenter = 0.0;
    demod->syncMagnitudeLate = 0.0;
    demod->syncPrevFrameOut = 0.0;

    demod->syncAcquisitionBestMagnitude = 0.0;
    demod->syncAcquisitionBestIndex = -1;

    demod->initCounter = DEMOD_INIT_DURATION;
    demod->syncAcquisitionCounter = DEMOD_FRAME_LENGTH;
    demod->syncFrameIndex = 0;
    demod->outputFrameIndex = 0;
    demod->scramblerState = 0;
    demod->syncFrameErrored = 0;
    demod->syncErrors = 0;
    demod->resyncFrameCounter = 0;

    // Reset the filter states.
    fir_reset(&demod->resamplingFilter);
    fir_reset(&demod->syncFilter);
    fir_reset(&demod->equalizerFilter);

    // Reset the equalizer.
    for (i = 0; i < DEMOD_EQUALIZER_LENGTH; i++)
        demod->equalizerFilter.taps[i] = (i == DEMOD_EQUALIZER_LENGTH / 2) ? 1.0 : 0.0;

    // Invoke the reset callback.
    if (demod->resetCallback)
        demod->resetCallback();
}

// Perform a symbol decision.
void demod_decision(demod_t *demod, demod_symbolDecision_t *decision, double complex symbol) {
    int i, numStates, bestState;
    double phaseSpan, phaseError, bestError;

    numStates = 1 << demod->bitsPerSymbol;
    phaseSpan = M_PI * 2.0 / numStates;

    // Find the closest phase state.
    bestState = -1;
    bestError = 0.0;
    phaseError = 0.0;
    for (i = 0; i < numStates; i++) {
        phaseError = fabs(carg(symbol * cexp(-1j * phaseSpan * i)));

        // New best.
        if (bestState == -1 || phaseError < bestError) {
            bestState = i;
            bestError = phaseError;
        }
    }

    // The confidence rating varies from 0 to 1.
    decision->confidence = 1.0 - bestError / phaseSpan * 2.0;

    // Map the state to the bits.
    switch(demod->bitsPerSymbol) {
        case 1:
            decision->bits = bestState;
            break;
        case 2:
            decision->bits = DEMOD_4PSK_MAPPING[bestState];
            break;
        case 3:
            decision->bits = DEMOD_8PSK_MAPPING[bestState];
            break;
    }

}

// Inputs a sample to the demodulator.
void demod_input(demod_t *demod, int16_t sample) {
    int i, j, scramblerFeedback;
    double complex mix, filterOut, syncOut, equalizerOut, scramblingSymbol, dataSymbol, referenceSymbol;
    double syncMagnitude, clockError;

    demod_symbolDecision_t symbolDecision;

    // Mix down and scale the input sample.
    mix = sample / 32768.0 / demod->agcLevel * cexp(-2.0 * I * demod->loPhase * M_PI);
    demod->loPhase = fmod(demod->loPhase + demod->loFrequency, 1.0);
    for (i = 0; i < DEMOD_INTERPOLATION; i++) {
        // Feed the sample into the resampling filter.
        fir_feed(&demod->resamplingFilter, i == 0 ? mix : 0.0);

        // Advance the clock.
        demod->clockPhase += DEMOD_BAUD_RATE / DEMOD_RATE;

        // Sample?
        if (demod->clockPhase >= 1.0) {
            demod->clockPhase = fmod(demod->clockPhase, 1.0);

            // Compute the output sample.
            filterOut = fir_mac(&demod->resamplingFilter);

            // Update the gain.
            demod->agcLevel += DEMOD_AGC_COEFF * (cabs(filterOut) / demod->agcLevel - demod->agcLevel);
            demod->agcLevel = clampDouble(demod->agcLevel, DEMOD_AGC_MIN, DEMOD_AGC_MAX);

            // Feed the sample into the sync and equalizer filters.
            syncOut = fir_next(&demod->syncFilter, filterOut);
            equalizerOut = fir_next(&demod->equalizerFilter, filterOut);

            // Current magnitude of the synchronization filter output.
            syncMagnitude = cabs(syncOut);

            // Current scrambling symbol.
            scramblingSymbol = cexp(1j * (demod->scramblerState & 7) * M_PI / 4);

            // Derotate the original data symbol.
            dataSymbol = equalizerOut * conj(scramblingSymbol);

            // Data output.
#ifdef DEBUG_PLOT
            printf("eqOut %f %f\n", creal(equalizerOut), cimag(equalizerOut));
            printf("syncMag %f\n", syncMagnitude);
            printf("level %f\n", demod->agcLevel);
#endif

            // Finished the acquisition?
            if (!demod->syncAcquisitionCounter) {
                if (demod->syncFrameIndex == DEMOD_SYNC_LENGTH - 1) {
                    demod->syncMagnitudeEarly = syncMagnitude;
                } else if (demod->syncFrameIndex == DEMOD_SYNC_LENGTH) {
                    demod->syncMagnitudeCenter = syncMagnitude;

                    // Apply a frequency correction based on how much the peak sync filter sample has rotated from the previous frame.
                    demod->loFrequency += DEMOD_FREQUENCY_COEFF * (carg(syncOut * conj(demod->syncPrevFrameOut)) - demod->loFrequency);
                    demod->loFrequency = clampDouble(demod->loFrequency, DEMOD_LO_MIN, DEMOD_LO_MAX);
                    demod->syncPrevFrameOut = syncOut;

                    // Output data.
                    log_printf("sync: magnitude %f\n", syncMagnitude);
#ifdef DEBUG_PLOT
                    printf("loFreq %f\n", demod->loFrequency * INPUT_RATE);
#endif
                } else if (demod->syncFrameIndex == DEMOD_SYNC_LENGTH + 1) {
                    demod->syncMagnitudeLate = syncMagnitude;

                    // Find the clocking error.
                    // Dividing by the center magnitude allows increasing sensitivity without too much noise (since a valid peak would lower the correction value).
                    clockError = (demod->syncMagnitudeEarly - demod->syncMagnitudeLate) / demod->syncMagnitudeCenter;

#ifdef DEBUG_PLOT
                    // Output data.
                    printf("clockError %f\n", clockError);
#endif

                    // Correction.
                    demod->clockPhase += DEMOD_CLOCK_COEFF * clockError;
                }

                // Sync sequence.
                if (demod->outputFrameIndex < DEMOD_SYNC_LENGTH) {
                    referenceSymbol = demod->syncFilter.taps[DEMOD_SYNC_LENGTH - 1 - demod->outputFrameIndex];

                    // Train the equalizer.
                    nlms_train(&demod->equalizerFilter, referenceSymbol, DEMOD_EQUALIZER_COEFF);

                    // Only lose sync when past the initial establishment timeout (to allow the AGC and equalizer to settle).
                    if (!demod->initCounter) {
                        // Compare the real parts of the PN sequence and the actual equalizer output.
                        // Do the signs differ? If yes, that's a mismatch.
                        if (creal(equalizerOut) * creal(referenceSymbol) <= 0.0) {
                            // More errors than the threshold allows?
                            if (++demod->syncErrors == DEMOD_SYNC_ERROR_THRESHOLD) {
                                // Mark this frame as errored (i.e. don't reset the resynchronization counter).
                                demod->syncFrameErrored = 1;
                                log_printf("sync: error threshold reached (streak = %d frames)\n", demod->resyncFrameCounter);

                                // A streak of errored frames? Reset.
                                if (++demod->resyncFrameCounter == DEMOD_SYNC_LOSS_FRAMES) {
                                    log_printf("sync: synchronization lost\n");
                                    demod_reset(demod);
                                    return;
                                }
                            }
                        }
                    }
                } else if (demod->outputFrameIndex == DEMOD_SYNC_LENGTH) {
                    log_printf("sync (agc=%f, lo=%f)\n", demod->agcLevel, demod->loFrequency * INPUT_RATE);

                    /*for (j = 0; j < DEMOD_EQUALIZER_LENGTH; j++)
                        log_printf("equalizer: %d = %f %f\n", j, creal(demod->equalizerFilter.taps[j]), cimag(demod->equalizerFilter.taps[j]));*/

                    // Reset the sync error counter.
                    demod->syncErrors = 0;

                    // If the frame wasn't errored, reset the streak.
                    if (!demod->syncFrameErrored)
                        demod->resyncFrameCounter = 0;

                    // Clear the flag.
                    demod->syncFrameErrored = 0;

                    // Reset the scrambler.
                    // This contradicts the standard: A-4 ("the generator is initialized to 1 at the start of each frame")
                    demod->scramblerState = 511;
                }

                // Past the sync prefix?
                if (demod->outputFrameIndex >= DEMOD_SYNC_LENGTH) {
#ifdef DEBUG_PLOT
                    printf("dataSym %f %f\n", creal(dataSymbol), cimag(dataSymbol));
#endif

                    if (((demod->outputFrameIndex - DEMOD_SYNC_LENGTH) % 48) >= 32) {
                        // Scrambled reference symbol, train the equalizer.
                        nlms_train(&demod->equalizerFilter, scramblingSymbol, DEMOD_EQUALIZER_COEFF);
                    } else {
                        // Data symbol.
                        if (demod->symbolCallback) {
                            demod_decision(demod, &symbolDecision, dataSymbol);
                            demod->symbolCallback(symbolDecision);
                        }
                    }
                }
            } else {
                // New best point?
                if (demod->syncAcquisitionBestIndex == -1 || syncMagnitude > demod->syncAcquisitionBestMagnitude) {
                    demod->syncAcquisitionBestMagnitude = syncMagnitude;
                    demod->syncAcquisitionBestIndex = demod->syncFrameIndex;
                    log_printf("sync: new best at %d (%f)\n", demod->syncAcquisitionBestIndex, demod->syncAcquisitionBestMagnitude);
                }

                // Done?
                if (--demod->syncAcquisitionCounter == 0) {
                    log_printf("sync: acquired\n");
                    // Align the frame index to track the new peak.
                    demod->syncFrameIndex = (DEMOD_FRAME_LENGTH + demod->syncFrameIndex - demod->syncAcquisitionBestIndex + DEMOD_SYNC_LENGTH) % DEMOD_FRAME_LENGTH;

                    // The equalizer output is delayed relative to the measured sync by a half of the equalizer's length samples.
                    demod->outputFrameIndex = (DEMOD_FRAME_LENGTH + demod->syncFrameIndex - DEMOD_EQUALIZER_LENGTH / 2) % DEMOD_FRAME_LENGTH;
                }
            }

            // Increment the counters.
            demod->syncFrameIndex = (demod->syncFrameIndex + 1) % DEMOD_FRAME_LENGTH;
            demod->outputFrameIndex = (demod->outputFrameIndex + 1) % DEMOD_FRAME_LENGTH;

            // Update the scrambler state.
            for (j = 0; j < 3; j++) {
                scramblerFeedback = ((demod->scramblerState >> 4) ^ demod->scramblerState) & 1;
                demod->scramblerState >>= 1;
                demod->scramblerState |= scramblerFeedback << 8;
            }

            // This counter allows a grace period during initial sync acquisition so that it isn't immediately lost.
            if (demod->initCounter)
                demod->initCounter--;
        }
    }
}

// Frees the memory allocated for the demodulator.
void demod_free(demod_t *demod) {
    fir_free(&demod->resamplingFilter);
    fir_free(&demod->syncFilter);
    fir_free(&demod->equalizerFilter);
}
