#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "demod.h"
#include "annex-e.h"
#include "log.h"

int bitsPerSerialFrame, bitsPerCharacter, useITA2, ita2Figs, serialBits, serialRaw, bitCounter, diagCounter;
demod_t demod;
annexE_decoder_t decoder;

// Output diagnostic data every N bits.
#define DIAGNOSTIC_FREQUENCY 256

#define ITA2_SWITCH_LTRS 0x1f
#define ITA2_SWITCH_FIGS 0x1b
const char ITA2_LTRS[32] = {
    '\0', 'E', '\n', 'A', ' ', 'S', 'I', 'U', '\r', 'D', 'R', 'J', 'N', 'F', 'C', 'K',
    'T', 'Z', 'L', 'W', 'H', 'Y', 'P', 'Q', 'O', 'B', 'G', '\0', 'M', 'X', 'V', '\0'
};

const char ITA2_FIGS[32] = {
    '\0', '3', '\n', '-', ' ', '\'', '8', '7', '\r', '$', '4', '\0', ',', '!', ':', '(',
    '5', '+', ')', '2', '#', '6', '0', '1', '9', '?', '&', '\0', '.', '/', '=', '\0'
};

void resetCallback() {
    log_printf("demod: reset callback\n");
    annexE_decoder_reset(&decoder);

    serialBits = 0;
    bitCounter = 0;
    ita2Figs = 0;
}

void symbolCallback(demod_symbolDecision_t decision) {
    annexE_decoder_input(&decoder, &decision);
}

void dataCallback(int bit) {
    int character;

    serialBits >>= 1;
    serialBits |= bit << (bitsPerSerialFrame - 1);

    if (++diagCounter == DIAGNOSTIC_FREQUENCY) {
        log_printf("viterbi quality: %.2f\n", decoder.viterbiLevel);
        diagCounter = 0;
    }

    bitCounter++;

    if (!serialRaw) {
        if (bitCounter == bitsPerSerialFrame) {
            bitCounter = 0;
            character = (serialBits >> 1) & ((1 << bitsPerCharacter) - 1);

            // Framing error (start bit should normally be equal to zero)
            if (serialBits & 1) {
                log_printf("tty: framing error\n");
                bitCounter += 1;
            }

            serialBits = 0;

            // ITA-2?
            if (useITA2) {
                // Yes, decode.
                if (character != ITA2_SWITCH_LTRS && character != ITA2_SWITCH_FIGS) {
                    printf("%c", ita2Figs ? ITA2_FIGS[character] : ITA2_LTRS[character]);
                } else {
                    // Switch letters / figures.
                    ita2Figs = character == ITA2_SWITCH_FIGS;
                }
            } else {
                // No, output it as ASCII.
                printf("%c", character);
            }
        }
    } else {
        if (bitCounter == 8) {
            printf("%02x ", serialBits);
            bitCounter = 0;
            serialBits = 0;
        }
    }
}

int main(int argc, char *argv[]) {
    int rate, decoderRate, demodRate, longInterleaving, stopBits;
    int16_t samp;

    ita2Figs = 0;
    serialBits = 0;
    bitCounter = 0;
    diagCounter = 0;
    serialRaw = 0;

    if (argc < 4) {
        log_printf("Usage: %s <rate> <long interleaving> <character format>\n", argv[0]);
        return EXIT_FAILURE;
    }

    rate = atoi(argv[1]);
    longInterleaving = atoi(argv[2]);

    switch (rate) {
        case 75:
            decoderRate = ANNEX_E_RATE_75;
            demodRate = DEMOD_RATE_1200;
            break;
        case 150:
            decoderRate = ANNEX_E_RATE_150;
            demodRate = DEMOD_RATE_1200;
            break;
        case 300:
            decoderRate = ANNEX_E_RATE_300;
            demodRate = DEMOD_RATE_1200;
            break;
        case 600:
            decoderRate = ANNEX_E_RATE_600;
            demodRate = DEMOD_RATE_1200;
            break;
        case 1200:
            decoderRate = ANNEX_E_RATE_1200;
            demodRate = DEMOD_RATE_2400;
            break;
        case 2400:
            decoderRate = ANNEX_E_RATE_2400;
            demodRate = DEMOD_RATE_3600;
            break;
        default:
            log_printf("Invalid data rate.\n");
            return EXIT_FAILURE;
    }

    // Character format.
    if (strlen(argv[3]) != 3) {
        log_printf("Invalid character format.\n");
        return EXIT_FAILURE;
    }

    if (strcmp(argv[3], "raw") != 0) {
        switch(argv[3][0]) {
            case '5':
                bitsPerCharacter = 5;
                useITA2 = 1;
                break;
            case '7':
                bitsPerCharacter = 7;
                useITA2 = 0;
                break;
            case '8':
                bitsPerCharacter = 8;
                useITA2 = 0;
                break;
            default:
                log_printf("Invalid character bit count.\n");
                return EXIT_FAILURE;
        }

        if (argv[3][1] != 'N') {
            log_printf("Parity checking isn't implemented, add a stop bit as a workaround.\n");
            return EXIT_FAILURE;
        }

        switch(argv[3][2]) {
            case '1':
                stopBits = 1;
                break;
            case '2':
                stopBits = 2;
                break;
            default:
                log_printf("Only 1 or 2 stop bits are correct.\n");
                return EXIT_FAILURE;
        }

        // Total bits per frame.
        bitsPerSerialFrame = 1 + bitsPerCharacter + stopBits;
        log_printf("tty: %d bits per serial character, ITA2=%d\n", bitsPerSerialFrame, useITA2);
    } else {
        serialRaw = 1;
        bitsPerSerialFrame = 8;
        log_printf("tty: raw output\n");
    }
    log_printf("debug: demod=%d decoder=%d long=%d\n", demodRate, decoderRate, longInterleaving);

    // Try creating the demodulator.
    if (!demod_alloc(&demod, demodRate, resetCallback, symbolCallback)) {
        log_printf("Couldn't allocate memory from the demodulator.\n");
        return EXIT_FAILURE;
    }

    // Try creating the STANAG 4285 Annex E decoder.
    if (!annexE_decoder_alloc(&decoder, decoderRate, longInterleaving, dataCallback)) {
        log_printf("Couldn't allocate memory for the Annex E decoder.\n");
        demod_free(&demod);
        return EXIT_FAILURE;
    }

    // Feed samples while they're available.
    while (fread(&samp, sizeof(int16_t), 1, stdin))
        demod_input(&demod, samp);

    annexE_decoder_free(&decoder);
    demod_free(&demod);
    return EXIT_SUCCESS;
}
