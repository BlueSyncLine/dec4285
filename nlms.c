#include <math.h>
#include <complex.h>
#include "fir.h"

// See https://en.wikipedia.org/wiki/Least_mean_squares_filter.
void nlms_train(firfilt_t *filter, double complex expectedSymbol, double learningRate) {
    int i, j;
    double complex error, accumulator;
    double normCoeff;

    accumulator = 0.0;
    normCoeff = 0.0;
    for (i = 0; i < filter->numTaps; i++) {
        j = (filter->currentIndex + filter->numTaps - i - 1) % filter->numTaps;
        accumulator += conj(filter->taps[i]) * filter->delay[j];
        normCoeff += pow(cabs(filter->delay[j]), 2);
    }

    error = expectedSymbol - accumulator;
    for (i = 0; i < filter->numTaps; i++) {
        j = (filter->currentIndex + filter->numTaps - i - 1) % filter->numTaps;
        filter->taps[i] += filter->delay[j] * learningRate * conj(error) / normCoeff;
    }
}
