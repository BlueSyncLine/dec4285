all: clean d4285

d4285:
	gcc -g -Og -Wall -Wextra -lm *.c -o d4285

clean:
	rm -f *.o d4285
