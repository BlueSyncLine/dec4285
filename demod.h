#ifndef DEMOD_H
#define DEMOD_H
#include <stdint.h>
#include "fir.h"
#include "util.h"
#include "params.h"

// STANAG 4285, A-1
#define DEMOD_CARRIER_FREQUENCY 1800.0
#define DEMOD_BAUD_RATE 2400.0

// STANAG 4285, A-5
#define DEMOD_CARRIER_TOLERANCE 75.0

// STANAG 4285, A-3-1
#define DEMOD_RRC_BETA  0.2

// STANAG 4285, A-3
#define DEMOD_FRAME_LENGTH 256

// STANAG 4285, A-8
#define DEMOD_SYNC_LENGTH 80

// Interpolation factor.
#define DEMOD_INTERPOLATION 3
#define DEMOD_RATE (INPUT_RATE * DEMOD_INTERPOLATION)

#define DEMOD_RRC_N 64
#define DEMOD_EQUALIZER_LENGTH 31

#define DEMOD_EQUALIZER_COEFF 0.95
#define DEMOD_AGC_COEFF 0.01
#define DEMOD_CLOCK_COEFF 0.03
#define DEMOD_FREQUENCY_COEFF 0.00005

#define DEMOD_AGC_MIN 0.0001
#define DEMOD_AGC_MAX 1.0
#define DEMOD_LO_MIN ((DEMOD_CARRIER_FREQUENCY - DEMOD_CARRIER_TOLERANCE) / INPUT_RATE)
#define DEMOD_LO_MAX ((DEMOD_CARRIER_FREQUENCY + DEMOD_CARRIER_TOLERANCE) / INPUT_RATE)
#define DEMOD_SYNC_ERROR_THRESHOLD 30
#define DEMOD_SYNC_LOSS_FRAMES 10
#define DEMOD_INIT_DURATION DEMOD_BAUD_RATE

// Basic modem rates (bits per symbol)
enum {
    DEMOD_RATE_1200 = 1,
    DEMOD_RATE_2400,
    DEMOD_RATE_3600
};

typedef struct {
    int bits;
    double confidence;
} demod_symbolDecision_t;

// Callback functions.
typedef void demod_resetCallback();
typedef void demod_symbolCallback(demod_symbolDecision_t decision);

typedef struct {
    int bitsPerSymbol;
    demod_resetCallback *resetCallback;
    demod_symbolCallback *symbolCallback;

    firfilt_t resamplingFilter;
    firfilt_t syncFilter;
    firfilt_t equalizerFilter;

    double loFrequency;
    double loPhase;
    double clockPhase;
    double agcLevel;

    double syncMagnitudeEarly;
    double syncMagnitudeCenter;
    double syncMagnitudeLate;
    double complex syncPrevFrameOut;

    double syncAcquisitionBestMagnitude;
    int syncAcquisitionBestIndex;

    int initCounter;
    int syncAcquisitionCounter;
    int syncFrameIndex;
    int outputFrameIndex;
    int scramblerState;

    int syncFrameErrored;
    int syncErrors;
    int resyncFrameCounter;
} demod_t;

int demod_alloc(demod_t *demod, int bitsPerSymbol, demod_resetCallback *resetCallback, demod_symbolCallback *symbolCallback);
void demod_reset(demod_t *demod);
void demod_decision(demod_t *demod, demod_symbolDecision_t *decision, double complex symbol);
void demod_input(demod_t *demod, int16_t sample);
void demod_free(demod_t *demod);
#endif
