#ifndef ANNEX_E_H
#define ANNEX_E_H

#define ANNEX_E_INTERLEAVER_ROWS 32
#define ANNEX_E_VITERBI_STATES 128
#define ANNEX_E_VITERBI_TRUNCATION 64

#include "demod.h"

enum {
    ANNEX_E_RATE_75,
    ANNEX_E_RATE_150,
    ANNEX_E_RATE_300,
    ANNEX_E_RATE_600,
    ANNEX_E_RATE_1200,
    ANNEX_E_RATE_2400
};

typedef struct {
    int bit;
    int previousState;
    double pathMetric;
} annexE_viterbiEntry_t;

typedef void annexE_dataCallback(int bit);

typedef struct {
    annexE_dataCallback *dataCallback;
    int dataRate;
    int bitsPerInputSymbol;
    int longInterleaving;

    double *deinterleaverDelayRows[ANNEX_E_INTERLEAVER_ROWS - 1];
    int deinterleaverDelayIndices[ANNEX_E_INTERLEAVER_ROWS - 1];
    double deinterleaverDelayOutputs[ANNEX_E_INTERLEAVER_ROWS];
    int deinterleaverJ;

    int deinterleaverIndex;

    annexE_viterbiEntry_t *viterbiEntries;
    int viterbiIndex;
    int viterbiSkipTracebacks;
    double viterbiLevel;
    int resetSkipBits;
} annexE_decoder_t;

int annexE_decoder_alloc(annexE_decoder_t *decoder, int dataRate, int longInterleaving, annexE_dataCallback *dataCallback);
void annexE_decoder_reset(annexE_decoder_t *decoder);
void annexE_decoder_inputBit(annexE_decoder_t *decoder, double softBit);
void annexE_decoder_input(annexE_decoder_t *decoder, const demod_symbolDecision_t *symbolDecision);
double annexE_decoder_bitMetric(int t1, int t2, double sT1, double sT2);
int annexE_decoder_viterbiNextState(int state, int bit);
int annexE_decoder_viterbiAdvance(annexE_decoder_t *decoder, double t1, double t2);
void annexE_decoder_free(annexE_decoder_t *decoder);
#endif
