#ifndef FIR_H
#define FIR_H
#include <complex.h>

typedef struct {
    double complex *taps;
    double complex *delay;
    int numTaps;
    int currentIndex;
} firfilt_t;

double sinc(double x);
double blackman(int M, int n);
void fir_lowpass_taps(double complex *taps, int n, double fc);
void fir_rrc_taps(double complex *taps, int n, double fc, double beta);

void fir_feed(firfilt_t *filt, double complex x);
double complex fir_mac(firfilt_t *filt);
double complex fir_next(firfilt_t *filt, double complex x);

void fir_reset(firfilt_t *filt);
int fir_zero_alloc(firfilt_t *filt, int numTaps);
int fir_lowpass_alloc(firfilt_t *filt, double sampleRate, double cutoff, int n);
int fir_rrc_alloc(firfilt_t *filt, double sampleRate, double baudRate, double beta, int n);
void fir_free(firfilt_t *filt);
#endif
