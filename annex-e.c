#include <malloc.h>
#include "annex-e.h"
#include "demod.h"
#include "log.h"


// Allocates memory for the decoder. Returns 1 on success and 0 on failure.
int annexE_decoder_alloc(annexE_decoder_t *decoder, int dataRate, int longInterleaving, annexE_dataCallback *dataCallback) {
    int i, currentRowLength;

    decoder->dataRate = dataRate;
    decoder->longInterleaving = longInterleaving;
    decoder->dataCallback = dataCallback;

    // STANAG 4285, E-2, E-4
    switch (dataRate) {
        case ANNEX_E_RATE_2400:
            decoder->deinterleaverJ = longInterleaving ? 48 : 4;
            decoder->bitsPerInputSymbol = 3;
            break;
        case ANNEX_E_RATE_1200:
            decoder->deinterleaverJ = longInterleaving ? 24 : 2;
            decoder->bitsPerInputSymbol = 2;
            break;
        default:
            decoder->deinterleaverJ = longInterleaving ? 12 : 1;
            decoder->bitsPerInputSymbol = 1;
            break;
    }

    // Allocate memory for Viterbi decoding.
    if (!(decoder->viterbiEntries = calloc(ANNEX_E_VITERBI_STATES * ANNEX_E_VITERBI_TRUNCATION, sizeof(annexE_viterbiEntry_t))))
        return 0;

    // Allow calling annexE_decoder_free to get rid of any allocated rows if the next step fails...
    for (i = 0; i < ANNEX_E_INTERLEAVER_ROWS - 1; i++)
        decoder->deinterleaverDelayRows[i] = NULL;

    // Try allocating the rows.
    // The last one has zero length and is thus not present.
    for (i = 0; i < ANNEX_E_INTERLEAVER_ROWS - 1; i++) {
        // The first row has 31J delay, the last has zero.
        currentRowLength = (ANNEX_E_INTERLEAVER_ROWS - i - 1) * decoder->deinterleaverJ;

        // Fail?
        if (!(decoder->deinterleaverDelayRows[i] = calloc(currentRowLength, sizeof(double)))) {
            annexE_decoder_free(decoder);
            return 0;
        }
    }

    // Initialize the variables, etc.
    annexE_decoder_reset(decoder);

    return 1;
}

// Resets the decoder.
void annexE_decoder_reset(annexE_decoder_t *decoder) {
    int i, j, currentRowLength;
    annexE_viterbiEntry_t *currentEntry;

    decoder->deinterleaverIndex = 0;
    decoder->viterbiIndex = 0;
    decoder->viterbiSkipTracebacks = ANNEX_E_VITERBI_TRUNCATION;
    decoder->viterbiLevel = 1.0;

    // The first bits of the output are skipped because the deinterleaver isn't full of valid data yet.
    // STANAG 4285, E-6
    switch (decoder->dataRate) {
        case ANNEX_E_RATE_2400:
            decoder->resetSkipBits = decoder->longInterleaving ? 24678 : 2150;
            break;
        case ANNEX_E_RATE_1200:
            decoder->resetSkipBits = decoder->longInterleaving ? 12390 : 1126;
            break;
        case ANNEX_E_RATE_600:
            decoder->resetSkipBits = decoder->longInterleaving ? 6246 : 614;
            break;
        case ANNEX_E_RATE_300:
            decoder->resetSkipBits = decoder->longInterleaving ? 3174 : 358;
            break;
        case ANNEX_E_RATE_150:
            decoder->resetSkipBits = decoder->longInterleaving ? 1638 : 230;
            break;
        case ANNEX_E_RATE_75:
            decoder->resetSkipBits = decoder->longInterleaving ? 870 : 166;
            break;
    }

    // Reset the deinterleaver.
    for (i = 0; i < ANNEX_E_INTERLEAVER_ROWS - 1; i++) {
        currentRowLength = (ANNEX_E_INTERLEAVER_ROWS - i - 1) * decoder->deinterleaverJ;

        for (j = 0; j < currentRowLength; j++)
            decoder->deinterleaverDelayRows[i][j] = 0.0;

        decoder->deinterleaverDelayIndices[i] = 0;
    }

    // Clear the Viterbi state.
    for (i = 0; i < ANNEX_E_VITERBI_STATES * ANNEX_E_VITERBI_TRUNCATION; i++) {
        currentEntry = &decoder->viterbiEntries[i];
        currentEntry->bit = -1;
        currentEntry->previousState = -1;
        currentEntry->pathMetric = 0.0;
    }
}

// Feed a soft bit decision into the decoder.
void annexE_decoder_inputBit(annexE_decoder_t *decoder, double softBit) {
    int i, j1, j2, currentRowLength, currentRowDelayIndex, viterbiOutBit, repeatCounter, numRepeats;
    double t1, t2;

    currentRowLength = (ANNEX_E_INTERLEAVER_ROWS - decoder->deinterleaverIndex - 1) * decoder->deinterleaverJ;

    // How many times are T1T2 repeated?
    switch (decoder->dataRate) {
        case ANNEX_E_RATE_75:
            numRepeats = 8;
            break;
        case ANNEX_E_RATE_150:
            numRepeats = 4;
            break;
        case ANNEX_E_RATE_300:
            numRepeats = 2;
            break;
        default:
            numRepeats = 1;
            break;
    }

    // For a non-zero delay length we use a row (deinterleaverIndex < 31).
    if (currentRowLength) {
        // Shift out a bit and place it in the output array.
        currentRowDelayIndex = decoder->deinterleaverDelayIndices[decoder->deinterleaverIndex];
        decoder->deinterleaverDelayOutputs[decoder->deinterleaverIndex] = decoder->deinterleaverDelayRows[decoder->deinterleaverIndex][currentRowDelayIndex];

        //log_printf("inputBit: rowLength=%d, deinterleaverIndex=%d, rowIndex=%d (bit=%f)\n", currentRowLength, decoder->deinterleaverIndex, currentRowDelayIndex, softBit);

        // Shift in the current bit.
        decoder->deinterleaverDelayRows[decoder->deinterleaverIndex][currentRowDelayIndex] = softBit;

        // Increment the index.
        decoder->deinterleaverDelayIndices[decoder->deinterleaverIndex] = (currentRowDelayIndex + 1) % currentRowLength;
    } else {
        // Otherwise, the bit goes straight through (row #31).
        decoder->deinterleaverDelayOutputs[decoder->deinterleaverIndex] = softBit;
    }

    // Increment the index.
    decoder->deinterleaverIndex++;
    decoder->deinterleaverIndex %= ANNEX_E_INTERLEAVER_ROWS;

    // Rollover, the output array contains valid bits which we can feed into the Viterbi decoder.
    if (!decoder->deinterleaverIndex) {
        repeatCounter = 0;
        t1 = 0.0;
        t2 = 0.0;

        // We take two bits out at once to feed into the Viterbi decoder.
        for (i = 0; i < ANNEX_E_INTERLEAVER_ROWS; i += 2) {
            // The bits are to be taken out in a modified sequence (STANAG 4285, E-5).
            j1 = (i * 9) % ANNEX_E_INTERLEAVER_ROWS;
            j2 = ((i + 1) * 9) % ANNEX_E_INTERLEAVER_ROWS;

            // Averaging.
            t1 += decoder->deinterleaverDelayOutputs[j1];
            t2 += decoder->deinterleaverDelayOutputs[j2];

            // Accumulated enough repeats.
            if (++repeatCounter == numRepeats) {
                // Input.
                viterbiOutBit = annexE_decoder_viterbiAdvance(decoder, t1 / numRepeats, t2 / numRepeats);

                // We have data!
                if (viterbiOutBit != -1 && decoder->dataCallback != NULL) {
                    // STANAG 4285, E-6
                    if (!decoder->resetSkipBits) {
                        decoder->dataCallback(viterbiOutBit);
                    } else {
                        decoder->resetSkipBits--;
                    }
                }

                // Prepare for the next symbol.
                t1 = 0.0;
                t2 = 0.0;
                repeatCounter = 0;
            }
        }
    }
}

// Map an incoming symbol onto soft bits (-1 ... 1), weighted by the decision confidence.
void annexE_decoder_input(annexE_decoder_t *decoder, const demod_symbolDecision_t *symbolDecision) {
    int i, bitSign;

    //log_printf("decoder input: %d, %f\n", symbolDecision->bits, symbolDecision->confidence);

    // Feed the bits in.
    for (i = decoder->bitsPerInputSymbol - 1; i >= 0; i--) {
        bitSign = (symbolDecision->bits >> i) & 1 ? 1 : -1;
        annexE_decoder_inputBit(decoder, symbolDecision->confidence * bitSign);
    }

    // For the 8-PSK mode, feed in an extra bit to compensate for puncturing.
    if (decoder->dataRate == ANNEX_E_RATE_2400)
        annexE_decoder_inputBit(decoder, 0.0);
}

// Compute the distance metric between the expected bits and the soft states.
double annexE_decoder_bitMetric(int t1, int t2, double sT1, double sT2) {
    int signT1, signT2;

    // Map onto signs.
    signT1 = t1 ? 1 : -1;
    signT2 = t2 ? 1 : -1;

    // Best: 0.0, worst: 4.0
    return 4.0 - (2.0 + signT1 * sT1 + signT2 * sT2);
}


// Advances the Viterbi decoding, outputs either a bit decision or -1 when no bit is available yet.
int annexE_decoder_viterbiAdvance(annexE_decoder_t *decoder, double t1, double t2) {
    int i, nextViterbiIndex, path, bit, nextState, transT1, transT2, tracebackIndex, bestPath;
    annexE_viterbiEntry_t *currentEntry, *nextEntry;
    double newMetric, bestMetric;

    nextViterbiIndex = (decoder->viterbiIndex + 1) % ANNEX_E_VITERBI_TRUNCATION;

    // Mark all potential next paths as invalid unless overwritten.
    for (path = 0; path < ANNEX_E_VITERBI_STATES; path++)
        decoder->viterbiEntries[nextViterbiIndex * ANNEX_E_VITERBI_STATES + path].previousState = -1;

    // Advance the decoder.
    for (path = 0; path < ANNEX_E_VITERBI_STATES; path++) {
        currentEntry = &decoder->viterbiEntries[decoder->viterbiIndex * ANNEX_E_VITERBI_STATES + path];

        // This path doesn't exist.
        // An exemption is made for the initial condition.
        if (currentEntry->previousState == -1 && !(decoder->viterbiSkipTracebacks == ANNEX_E_VITERBI_TRUNCATION && path == 0))
            continue;

        // Two possible paths.
        for (bit = 0; bit < 2; bit++) {
            // The corresponding next state.
            nextState = (path >> 1) | (bit << 6);

            // Pointer to the next entry.
            nextEntry = &decoder->viterbiEntries[nextViterbiIndex * ANNEX_E_VITERBI_STATES + nextState];

            // STANAG 4285, E-1
            // Polynomial 133 (oct) for T1.
            transT1 = ((nextState >> 6) ^ (nextState >> 4) ^ (nextState >> 3) ^ (nextState >> 1) ^ nextState) & 1;

            // Polynomial 171 (oct) for T2.
            transT2 = ((nextState >> 6) ^ (nextState >> 5) ^ (nextState >> 4) ^ (nextState >> 3) ^ nextState) & 1;

            // Tweak: use an IIR filter so that the metric doesn't overflow (mentioned in Texas Instruments' SPRA099).
            // The coefficient had to be increased from 0.9 to 0.99 for more sensitive quality levels.
            newMetric = 0.99 * currentEntry->pathMetric + 0.01 * annexE_decoder_bitMetric(transT1, transT2, t1, t2);
            //newMetric = currentEntry->pathMetric + annexE_decoder_bitMetric(transT1, transT2, t1, t2);

            // Only overwrite an entry if the metric would become better.
            if (nextEntry->previousState == -1 || newMetric < nextEntry->pathMetric) {
                nextEntry->bit = bit;
                nextEntry->previousState = path;
                nextEntry->pathMetric = newMetric;
            }
        }
    }

    decoder->viterbiIndex = nextViterbiIndex;
    tracebackIndex = decoder->viterbiIndex;

    // Perform traceback?
    if (!decoder->viterbiSkipTracebacks) {
        // Find the best path.
        bestPath = -1;
        bestMetric = 0.0;

        for (path = 0; path < ANNEX_E_VITERBI_STATES; path++) {
            currentEntry = &decoder->viterbiEntries[tracebackIndex * ANNEX_E_VITERBI_STATES + path];

            // No such path.
            if (currentEntry->previousState == -1)
                continue;

            //log_printf("viterbi: %d -> %f\n", path, currentEntry->pathMetric);
            if (bestPath == -1 || currentEntry->pathMetric < bestMetric) {
                bestPath = path;
                bestMetric = currentEntry->pathMetric;
            }
        }

        // No best path? What the hell?
        if (bestPath == -1)
            log_printf("viterbi: this should never happen!\n");

        //log_printf("viterbi: best path %d, best metric %f, idx=%d\n", bestPath, bestMetric, tracebackIndex);

        // Set the signal quality level variable.
        decoder->viterbiLevel = bestMetric / 4.0;

#ifdef DEBUG_PLOT
        printf("viterbi %f\n", bestMetric);
#endif

        // Traceback.
        path = bestPath;
        for (i = 0; i < ANNEX_E_VITERBI_TRUNCATION - 1; i++) {
            currentEntry = &decoder->viterbiEntries[tracebackIndex * ANNEX_E_VITERBI_STATES + path];
            path = currentEntry->previousState;
            tracebackIndex = (ANNEX_E_VITERBI_TRUNCATION + tracebackIndex - 1) % ANNEX_E_VITERBI_TRUNCATION;
        }

        // Another assertion.
        if (currentEntry->bit == -1)
            log_printf("viterbi: current entry is uninitialized?!\n");

        return currentEntry->bit;
    } else {
        // Don't do traceback yet, so no bit is available.
        decoder->viterbiSkipTracebacks--;
        return -1;
    }
}

// Free up the allocated memory.
void annexE_decoder_free(annexE_decoder_t *decoder) {
    int i;
    free(decoder->viterbiEntries);

    for (i = 0; i < ANNEX_E_INTERLEAVER_ROWS - 1; i++)
        free(decoder->deinterleaverDelayRows[i]);
}
