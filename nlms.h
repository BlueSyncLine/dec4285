#ifndef NLMS_H
#define NLMS_H
#include <complex.h>
#include "fir.h"

void nlms_train(firfilt_t *filter, double complex expectedSymbol, double learningRate);
#endif
